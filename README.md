## Test EON

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
### 1-PHP  
Once the project is cloned.
Run:
composer install

Next, create .env file.

Inside the Controllers folder (App \ Controller) is the PHP ScoreDataIndexerController which implements ScoreDataIndexerInterface found in the Util folder (App \ Util).

Unit test:
App \ Util

Inside the Test (App \ Test) folder is the unit test of the getCountOfUsersWithinScoreRange and getCountOfUsersByCondition functions.


### 2-Database  
Inside the sql folder you will find the database dump (eon_test.sql) and the query (statement.sql).

Also, migrations were created, which create the branch and loan tables, execute:

php bin / console doctrine: migrations: execute migrations / Version20210108184344.php

Or, just run the SQL files in your database manager.

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

### 1-PHP  
Una vez clonado el proyecto.
Ejecutar:
composer install

Despues, crear archivo .env.

Dentro de la carpeta de Controladores (App\Controller) se encuentra el PHP ScoreDataIndexerController el cual implementa ScoreDataIndexerInterface que se encuentra en la carpeta Util (App\Util).

Pruba unitaria:
App\Util

Dentro de la carpeta Test (App\Test) se encuetra la pruba unidatara de las funciones getCountOfUsersWithinScoreRange y getCountOfUsersByCondition.


### 2-Database  
Dentro de la carpeta sql se encuentra el dump de la base de datos (eon_test.sql) y el query (statement.sql).

Tambien, se crearon migraciones, las cuales crean las tablas de rama y prestamo, ejecutar:

php bin/console doctrine:migrations:execute migrations/Version20210108184344.php 

O, simplemente ejecutar los archivos SQL en tu manejador de base de datos.

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------