<?php

namespace App\Test;

use App\Controller\ScoreDataIndexerController;
use PHPUnit\Framework\TestCase;

class ScoreDataIndexerTest extends TestCase
{
    public function test()
    {
    	$index = new ScoreDataIndexerController();

        $this->assertEquals(4, $index->getCountOfUsersWithinScoreRange(20, 50));
        $this->assertEquals(1, $index->getCountOfUsersWithinScoreRange(-40, 0));
        $this->assertEquals(5, $index->getCountOfUsersWithinScoreRange(0, 80));
        $this->assertEquals(1, $index->getCountOfUsersByCondition('CA', 'w', false, false));
        $this->assertEquals(0, $index->getCountOfUsersByCondition('CA', 'w', false, true));
        $this->assertEquals(1, $index->getCountOfUsersByCondition('CA', 'w', true, true));
        $this->assertEquals(2, $index->getCountOfUsersByCondition('NY', 'm', true, true));
    }
}