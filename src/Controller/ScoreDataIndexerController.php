<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Util\ScoreDataIndexerInterface;

class ScoreDataIndexerController extends AbstractController implements ScoreDataIndexerInterface
{
    private $data;

    function __construct()
    {
        $this->data = $this->getCvsData();
    }

    public function getCountOfUsersWithinScoreRange(
        int $rangeStart,
        int $rangeEnd): int
    {
        $count = 0;

        foreach (json_decode($this->data) as $data) {
            if ( $data->Score >= $rangeStart && $data->Score <= $rangeEnd ) {
                $count++;
            }
        }

        return $count;
    }

    public function getCountOfUsersByCondition(
        string $region,
        string $gender,
        bool $hasLegalAge,
        bool $hasPositiveScore
    ): int
    {
        $count = 0;

        foreach (json_decode($this->data) as $data) {

            if ($hasLegalAge && $hasPositiveScore) {
                if ( $data->Region == $region && $data->Gender == $gender && $data->Age >= 18 && $data->Score >= 1 ) {
                    $count++;
                }
            }else if (!$hasLegalAge && $hasPositiveScore) {
                if ( $data->Region == $region && $data->Gender == $gender && $data->Age <= 18 && $data->Score >= 1 ) {
                    $count++;
                }
            }else if ($hasLegalAge && !$hasPositiveScore) {
                if ( $data->Region == $region && $data->Gender == $gender && $data->Age >= 18 && $data->Score <= 0 ) {
                    $count++;
                }
            }else if (!$hasLegalAge && !$hasPositiveScore) {
                if ( $data->Region == $region && $data->Gender == $gender && $data->Age <= 18 && $data->Score <= 0 ) {
                    $count++;
                }
            }
            
        }

        return $count;
    }

    private function getCvsData()
    {   
        if (($csv = fopen('./public/example.csv', "r")) === false)
        {
            die("Can't open the file.");
        }
        $headers = fgetcsv($csv, 4000, ",");
        $json = array();

        while ($row = fgetcsv($csv, 4000, ","))
        {
            $json[] = array_combine($headers, $row);
        }

        fclose($csv);
        return json_encode($json);
    }
}
