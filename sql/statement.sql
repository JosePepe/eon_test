/*Query by state*/
SELECT br.country, 
	br.state, 
	lo.is_active,
	COUNT(lo.is_active) as total,
	ROUND(
		CASE
			WHEN lo.is_active = 0 THEN 
				(
					(
						SELECT COUNT(br1.id) 
						FROM branch br1 
						JOIN loan lo on br1.id = lo.branch_id 
						WHERE lo.is_active = 0 AND br1.state = br.state 
						GROUP BY br1.state
					) * 100) / (
									SELECT COUNT(br0.id) 
									FROM branch br0 
									JOIN loan lo on br0.id = lo.branch_id 
									WHERE br0.state = br.state 
									GROUP BY br0.state
								)
			WHEN lo.is_active = 1 THEN 
				(
					(
						SELECT COUNT(br3.id) 
						FROM branch br3 
						JOIN loan lo on br3.id = lo.branch_id 
						WHERE lo.is_active = 1 AND br3.state = br.state 
						GROUP BY br3.state
					) * 100) / (
									SELECT COUNT(br2.id) 
									FROM branch br2 
									JOIN loan lo on br2.id = lo.branch_id 
									WHERE br2.state = br.state 
									GROUP BY br2.state
								)
		END
	,2) as percentage
FROM branch br 
JOIN loan lo on br.id = lo.branch_id
GROUP BY lo.is_active, br.state
ORDER BY br.state;

/*Query by country*/
SELECT br.country,
	lo.is_active,
	COUNT(lo.is_active) as total,
	ROUND(
		CASE
			WHEN lo.is_active = 0 THEN 
				(
					(
						SELECT COUNT(br1.id) 
						FROM branch br1 
						JOIN loan lo on br1.id = lo.branch_id 
						WHERE lo.is_active = 0
						GROUP BY br1.country
					) * 100) / (
									SELECT COUNT(br0.id) 
									FROM branch br0 
									JOIN loan lo on br0.id = lo.branch_id 
									GROUP BY br0.country
								)
			WHEN lo.is_active = 1 THEN 
				(
					(
						SELECT COUNT(br3.id) 
						FROM branch br3 
						JOIN loan lo on br3.id = lo.branch_id 
						WHERE lo.is_active = 1
						GROUP BY br3.country
					) * 100) / (
									SELECT COUNT(br2.id) 
									FROM branch br2 
									JOIN loan lo on br2.id = lo.branch_id 
									GROUP BY br2.country
								)
		END
	,2) as percentage
FROM branch br 
JOIN loan lo on br.id = lo.branch_id
GROUP BY lo.is_active,br.country;